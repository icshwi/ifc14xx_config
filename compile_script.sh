#!/bin/bash

mkdir -p build
mkimage -T script -C none -n 'U-Boot clear env script' -d uboot_clear_variables.script build/uboot_clear_variables.img
mkimage -T script -C none -n 'U-Boot update script' -d uboot_update_uboot.script build/uboot_update_uboot.img
mkimage -T script -C none -n 'Env update and backup script' -d uboot_update_env_and_backup.script build/uboot_update_env_and_backup.img
mkimage -T script -C none -n 'FBIS PON update script' -d uboot_update_pon_fbis.script  build/uboot_update_pon_fbis.img

