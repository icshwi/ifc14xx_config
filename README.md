IFC14xx initial configuration script

```
$ ./ifc14xx_config.sh

Usage: ./ifc14x$ ./ifc14xx_config.shx_config.sh <device> <baud> [options]

All arguments are positional.

Arguments:
  <device>   - UART serial connection of the IFC14xx device. (default: /dev/ttyUSB0)
  <baud>     - Baud rate of the UART connection.             (default: 115200)

Optional parameters:
  --fbis_pon - Flag to signal that the PON microcode should be upgraded.
               For FBIS systems only.

NOTE: This script requires the IFC14xx device to be halted at the U-Boot console prompt
      See: https://confluence.esss.lu.se/display/HAR/Useful+Information#UsefulInformation-ConfigureU-BootenvironmentofIFC14xx
      for details.

Examples:
  ./ifc14xx_config /dev/ttyUSB0 115200
  Run the script with default values to configure the IFC14xx device connected to
  the UART serial device at /dev/ttyUSB0 with a baud rate of 115200

  ./ifc14xx_config /dev/ttyUSB1 115200
  Configure the IFC14xx device at /dev/ttyUSB1 with a baud rate of 115200

  ./ifc14xx_config /dev/ttyUSB0 115200 --fbis_pon
  Configure the IFC14xx device at /dev/ttyUSB0, a baud rate of 115200
  and upgrade the PON FPGA microcode for FBIS systems.
```
