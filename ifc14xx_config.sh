#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <device> <baud> [options]"
    echo ""
    echo "All arguments are positional."
    echo ""
    echo "Arguments:"
    echo "  <device>   - UART serial connection of the IFC14xx device. (default: /dev/ttyUSB0)"
    echo "  <baud>     - Baud rate of the UART connection.             (default: 115200)"
    echo ""
    echo "Optional parameters:"
    echo "  --fbis_pon - Flag to signal that the PON microcode should be upgraded."
    echo "               For FBIS systems only."
    echo ""
    echo "NOTE: This script requires the IFC14xx device to be halted at the U-Boot console prompt"
    echo "      See: https://confluence.esss.lu.se/display/HAR/Useful+Information#UsefulInformation-ConfigureU-BootenvironmentofIFC14xx"
    echo "      for details."
    echo ""
    echo "Examples:"
    echo "  ./ifc14xx_config /dev/ttyUSB0 115200"
    echo "  Run the script with default values to configure the IFC14xx device connected to"
    echo "  the UART serial device at /dev/ttyUSB0 with a baud rate of 115200"
    echo ""
    echo "  ./ifc14xx_config /dev/ttyUSB1 115200"
    echo "  Configure the IFC14xx device at /dev/ttyUSB1 with a baud rate of 115200"
    echo ""
    echo "  ./ifc14xx_config /dev/ttyUSB0 115200 --fbis_pon"
    echo "  Configure the IFC14xx device at /dev/ttyUSB0, a baud rate of 115200"
    echo "  and upgrade the PON FPGA microcode for FBIS systems."
    exit 1
fi

if [ -z "$1" ]; then
	device="/dev/ttyUSB0"
else
	device="$1"
fi

if [ -z "$2" ]; then
	baud="115200"
else
	baud="$2"
fi

if [ -z "$3" ]; then
    update_pon="false"
else
    if [ "$3" = "--fbis_pon" ]; then
        update_pon="true"
    else
        update_pon="false"
    fi
fi

stty -F $device speed $baud

echo "Configure IFC14XX board for deployment"

echo "U-boot clear env variables"
echo -ne "dhcp\r" > $device
sleep 3
echo -ne "tftp 1000000 172.30.4.88:ifc14xx_config/uboot_clear_variables.img\r" > $device
sleep 3
echo -ne "source 1000000\r" > $device
sleep 10

echo "Update U-boot"
echo -ne "tftp 1000000 172.30.4.88:ifc14xx_config/uboot_update_uboot.img\r" > $device
sleep 3
echo -ne "source 1000000\r" > $device
sleep 30

echo "Update U-boot environment and write backup to prom.4"
echo -ne "tftp 1000000 172.30.4.88:ifc14xx_config/uboot_update_env_and_backup.img\r" > $device
sleep 3
echo -ne "source 1000000\r" > $device
sleep 10

if [ "$update_pon" = "true" ]; then
    echo "Update PON FPGA microcode for FBIS systems, and reboot"
    # For some reason a simple command is needed here to clear the
    # buffer (or something)
    echo -ne "?\r" > $device
    sleep 3
    echo -ne "tftp 1000000 172.30.4.88:ifc14xx_config/uboot_update_pon_fbis.img\r" > $device
    sleep 3
    echo -ne "source 1000000\r" > $device
    sleep 30
else
    echo "Rebooting IFC14XX device"
    echo -ne "reset\r" > $device
    sleep 30
fi
